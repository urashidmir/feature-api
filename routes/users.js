const express = require("express");
const router = express.Router();

const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");
const path = require("path");
const adapter = new FileSync("./db/data.json");

const db = low(adapter);

router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.get("/:email", function (req, res, next) {

  const userInfo = db.get("users").find({ email: req.params.email }).value();
  const location = userInfo ? userInfo.location : null;
  const features = db.get("features").value();
  const userFeatures = features.filter(
    (feature) =>
      feature.enabledEmails.includes(
        req.params.email
      )
  );

  const locationFeatures = location
    ? features.filter((feature) =>
        feature.includedCountries.includes(location)
      )
    : [];

  const allFeatures = new Set([
    ...userFeatures,
    ...locationFeatures
  ]);

  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify({ "Available features": [... allFeatures] }));
});


module.exports = router;
