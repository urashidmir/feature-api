//During the test the env variable is set to test
process.env.NODE_ENV = "test";

//Require the dev-dependencies
let chai = require("chai");
let chaiHttp = require("chai-http");
let app = require("../app");
let should = chai.should();

chai.use(chaiHttp);


/*
  * Test the /GET route
  */
describe('/GET feature', () => {
    it('it should GET all the features', (done) => {
      chai
        .request(app)
        .get("/users/fred@example.com")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });
});


