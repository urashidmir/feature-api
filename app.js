const express = require("express");
const usersRouter = require("./routes/users.js");

const app = express();
const port = 3000;

app.use("/users", usersRouter);

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

module.exports = app;
